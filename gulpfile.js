var gulp              = require('gulp'),
      sass              = require('gulp-sass'),
      browserSync = require('browser-sync'),
      cssnano        = require('gulp-cssnano'),
      autoprefixer  = require('gulp-autoprefixer');

gulp.task('scss', function(){
    return gulp.src('src/css/style.scss')
        .pipe(sass())
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(cssnano())
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});

gulp.task('watch', ['browser-sync', 'scss'], function() {
    gulp.watch('src/css/**/*.scss', ['scss']);

});

gulp.task('default', ['watch']);